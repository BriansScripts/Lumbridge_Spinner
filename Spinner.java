import impl.useBank;
import impl.useWheel;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.event.listeners.RenderListener;
import org.rspeer.runetek.event.types.RenderEvent;
import org.rspeer.script.ScriptMeta;
import org.rspeer.script.task.Task;
import org.rspeer.script.task.TaskScript;

import java.awt.*;

@ScriptMeta(name = "Lumbridge Spinner",  desc = "STOPPING WHEN OUT OF FLAX WILL BE TEMPORARILY REMOVED", developer = "Brian")
public class Spinner extends TaskScript {

    private static final Task[] TASKS = { new useBank(), new useWheel()};

    @Override
    public void onStart() {
        submit(TASKS);
    }

    @Override
    public void onStop() {

    }

}