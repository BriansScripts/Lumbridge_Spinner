package impl;

import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.Movement;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class useWheel extends Task {

    public boolean validate() {
        return (Inventory.contains("Flax"));
    }

    public int execute() {
        if (!Interfaces.isOpen(270)) {
            SceneObject wheel = SceneObjects.getNearest("Spinning wheel");
            if (wheel != null) {
                if (wheel.isPositionWalkable()) {
                    //Click on wheel
                    if (wheel.interact("Spin")) {
                        Time.sleepUntil(() -> !Players.getLocal().isMoving(), 5000);
                        //needs a small sleep after opening the spinning interace, if it interacts too quickly nothing will happen
                        Time.sleep(350);
                    }
                } else {
                    //open door to room
                    SceneObject door = SceneObjects.getFirstAt(new Position(3207, 3214, 1));
                    if (door != null) {
                        if (door.interact("Open")) {
                            Time.sleepUntil(() -> wheel.isPositionWalkable(), 3000);
                        }
                    }
                }
            } else if (Players.getLocal().getFloorLevel() == 2) {
                //currently upstairs
                SceneObject stairs = SceneObjects.getFirstAt(new Position(3205, 3208, 2));
                if (stairs != null) {
                    if (Movement.getRunEnergy() > 25 && !Movement.isRunEnabled()){
                        Movement.toggleRun(true);
                        Time.sleep(100,150);
                    }
                    if (stairs.interact("Climb-down")) {
                        Time.sleepUntil(() -> Players.getLocal().getFloorLevel() == 1, 6500);
                    }
                }
            }
        } else {
            //Interact with interface to spin bowstrings
            if (Interfaces.getComponent(270, 16).interact("Spin")) {
                Time.sleepUntil(() -> !Inventory.contains("Flax"), () -> Players.getLocal().getAnimation() != -1, 125, 2500);
            }
        }
        return 100;
    }
}
