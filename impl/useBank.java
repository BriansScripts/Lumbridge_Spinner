package impl;

import org.rspeer.runetek.adapter.scene.Npc;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.BankLocation;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Bank;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.movement.position.Position;
import org.rspeer.runetek.api.scene.Npcs;
import org.rspeer.runetek.api.scene.Players;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;
import org.rspeer.ui.Log;

public class useBank extends Task {

    public boolean validate(){
        return !Inventory.contains("Flax");
    }

    public int execute(){
        if (Players.getLocal().getFloorLevel() == 1){
            SceneObject stairs = SceneObjects.getFirstAt(new Position(3204, 3207, 1));
            if (stairs != null) {

                if (stairs.isPositionWalkable()) {
                    if (stairs.interact("Climb-up")) {
                        Time.sleepUntil(() -> Players.getLocal().getFloorLevel() == 2, 6500);
                    }
                }else{
                    SceneObject door = SceneObjects.getFirstAt(new Position(3207,3214,1));
                    if (door != null){
                        if (door.interact("Open")){
                            Time.sleepUntil(()-> stairs.isPositionWalkable(),3000);
                        }
                    }
                }
            }
        }else if (Players.getLocal().getFloorLevel() == 2){
            if (!Bank.isOpen()){
                SceneObject banker = SceneObjects.getNearest("Bank booth");
                if (banker != null){
                    if (banker.interact("Bank")) {
                        Time.sleepUntil(() -> Bank.isOpen(), 5500);
                    }
                }
            }else{
                if (!Inventory.isEmpty()) {
                    if (Bank.depositInventory()){
                        if (!Bank.contains("Flax") && Bank.contains("Bow string")){
                            //Log.info("Out of flax, stopping script");
                            //Add a logout
                            //if (Players.getLocal().)
                            //return -1;
                        }
                        Time.sleepUntil(()-> Inventory.isEmpty(),1500);
                    }
                }
                if (Bank.withdrawAll("Flax")){
                    Time.sleepUntil(()->Inventory.contains("Flax"),1500);
                    return 25;
                }

            }
        }
        return 100;
    }
}
